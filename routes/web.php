<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginsController;
use App\Http\Controllers\RegistroController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PeticionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/homeback', [HomeController::class, 'indexBackend'])->name('home.indexBackend');
Route::get('/home', [HomeController::class, 'indexHome'])->name('home.indexHome');


Route::get('/login', [LoginsController::class, 'index'])->name('login.index');
Route::post('/login', [LoginsController::class, 'store'])->name('login.store');

Route::get('/loginbackend', [LoginsController::class, 'indexback'])->name('loginback.indexback');
Route::post('/loginbackend', [LoginsController::class, 'storeback'])->name('loginback.storeback');

Route::get('/registro', [RegistroController::class, 'index'])->name('registro.index');
Route::post('/registro', [RegistroController::class, 'store'])->name('registro.store');

Route::resource('homeback/users', UserController::class)->names('backend.users');



Route::get('/nuevoservicios', [PeticionController::class, 'index'])->name('peticion.index');
        Route::post('/nuevoservicios', [PeticionController::class, 'store'])->name('peticion.store');

Route::get('/servicios', [PeticionController::class, 'indexServicio'])->name('peticionServicio.index');
Route::post('/servicios/ver/{id}', [PeticionController::class, 'show'])->name('producto.show');