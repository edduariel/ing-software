<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1=Role::create(['name'=>'Admin']);
        $role2=Role::create(['name'=>'Cliente']);
        $role3=Role::create(['name'=>'Prestamista']);

        Permission::create(['name'=>'home.indexBackend'])->assingRole($role1);
        Permission::create(['name'=>'home.indexCliente'])->assingRole($role2);
        Permission::create(['name'=>'home.indexPrestamista'])->assingRole($role3);
    }
}
