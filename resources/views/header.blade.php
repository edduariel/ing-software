<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Title -->
	<title>ComuService</title>
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css2?family=Cabin:wght@400;500;600;700&display=swap" rel="stylesheet">
	<!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
	<!-- Bootstrap Min CSS -->
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<!-- Animate Min CSS -->
	<link rel="stylesheet" href="{{ asset('css/animate.min.css') }}">
	<!-- Pe-icon-7 CSS -->
	<link rel="stylesheet" href="{{ asset('css/pe-icon-7-stroke.css') }}">
	<!-- Font Awesome Min CSS -->
	<link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
	<!-- Mran Menu CSS -->
	<link rel="stylesheet" href="{{ asset('css/meanmenu.css') }}">
	<!-- Owl Carousel Min CSS -->
	<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
	<!-- Style CSS -->
    
	<link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/prueba.css') }}">
	<!-- Responsive CSS -->
	<link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <link href="//db.onlinewebfonts.com/c/7218a2da5a03f622ca18085806382043?family=Pe-icon-7-stroke" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="path/to/pe-icon-7-stroke/css/pe-icon-7-stroke.css">

<!-- Optional - Adds useful class to manipulate icon font display -->
<link rel="stylesheet" href="path/to/pe-icon-7-stroke/css/helper.css">
    
</head>
<body>

	<!-- Start Preloader Area -->
	
			

	<!-- End Preloader Area -->
	
	<!-- Start Navbar Area -->
	@guest
	<div class="navbar-area">
		<div class="techmax-responsive-nav index-navber-responsive">
			<div class="container">
				<div class="techmax-responsive-menu">
					<div class="logo">
						<a href="index.html">
							ComuService
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="techmax-nav index-navber">
			<div class="container">
				<nav class="navbar navbar-expand-md navbar-light">
					<a class="navbar-brand" href="index.html">
                        ComuService
					</a>
					<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
						<ul class="navbar-nav">
							<li class="nav-item"> 
								<a href="index.html" class="nav-link">Home</a>
							</li>
							<li class="nav-item">
								<a href="about.html" class="nav-link">Sobre Nosotros</a>
							</li>
							<li class="nav-item">
								<a href="{{ asset('servicios')}}" class="nav-link">Servicios </a>
								
							</li>
							
							<li class="nav-item">
								<a href="contact.html" class="nav-link">Contacto</a>
							</li>
						</ul>
						<div class="other-option">
							<a class="default-btn nav-btn-1" href="{{ asset('registro')}}">Registro <span></span></a>
							<a class="default-btn" href="{{ asset('login')}}">Iniciar Sesion <span></span></a>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
	@endguest
	@auth
	<div class="navbar-area">
		<div class="techmax-responsive-nav index-navber-responsive">
			<div class="container">
				<div class="techmax-responsive-menu">
					<div class="logo">
						<a href="index.html">
							ComuService
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="techmax-nav index-navber">
			<div class="container">
				<nav class="navbar navbar-expand-md navbar-light">
					<a class="navbar-brand" href="index.html">
                        ComuService
					</a>
					<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
						<ul class="navbar-nav">
							<li class="nav-item"> 
								<a href="{{ asset('home')}}" class="nav-link">Home</a>
							</li>
							<li class="nav-item">
								<a href="about.html" class="nav-link">Sobre Nosotros</a>
							</li>
							<li class="nav-item">
								<a href="{{ asset('nuevoservicios')}}" class="nav-link"> Nuevo Servicio </a>
								
							</li>
							<li class="nav-item">
								<a href="{{ asset('servicios')}}" class="nav-link">Servicios </a>
								
							</li>
							
							<li class="nav-item">
								<a href="contact.html" class="nav-link">Contacto</a>
							</li>
						</ul>
						<div class="other-option">
							
							<a class="default-btn" >Bienvenido {{auth()->user()->name }} <span></span></a>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>

	@endauth