@extends('adminlte::page')

@section('title', 'ComuService')

@section('content_header')
    <h1>ComuService</h1>
@stop

@section('content')
    <p>Bienvenido al panel de configuracion de ComuService</p>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hola ComuService te saluda'); </script>
@stop