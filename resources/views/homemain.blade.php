
@include('header')

	<!-- End Navbar Area -->
	
	<!-- Start Home Area -->
	<div class="home-area">
		<div class="d-table">
			<div class="d-table-cell">
				<div class="container">
					<div class="row align-items-center">
						<div class="col-lg-6 col-md-12">
							<div class="main-banner-content">
								<h1>Encargados en solucionar problemas cotidianos <span class="color-text">Con Tecnologia</span></h1>
								<p>Somos un grupo de estudiantes de la Universidad Laica Eloy Alfaro, en el cual proponemos nuestra app llamada ComuService</p>
								<div class="banner-btn">
									<a href="about.html" class="default-btn-one">
                                        Leer Más
                                        <span></span>
                                    </a>
									<a class="default-btn" href="contact.html">
                                        Contactanos
                                        <span></span>
                                    </a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-12">
							<div class="banner-image">
								<img src="{{ asset('images/Entrada-Uleam.jpg') }}" alt="image">
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<div class="grey-cr"></div>
	</div>
	<!-- End Home Area -->
	
	<!-- Start Services Section -->
	<section class="services-section pt-70 pb-100">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<i class="pe-7s-tools"></i>
						</div>
						<h3>Soluciones Tecnologicas</h3>
						<p>Brindamos las mejores soluciones informaticas</p>
						
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<i class="pe-7s-display1"></i>
						</div>
						<h3>Desarrollo Web</h3>
						<p>Tu negocio al alcance de un solo click al mundo</p>
						
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services-item">
						<div class="services-icon">
							<i class="pe-7s-share"></i>
						</div>
						<h3>Servicio de Redes</h3>
						<p>Contamos con los mejores topologias para tu empresa</p>
						
					</div>
				</div>
			</div>
		</div>
	</section>

	

	<!-- Start Team Section -->
	<section class="team-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h2>Miembros del Equipo</h2>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="{{ asset('images/WhatsApp Image 2022-12-14 at 23.10.53 (1).jpeg') }}" alt="team" />
							
						</div>
						<div class="team-info">
							<h3>Alexis Chele</h3>
							<span>Backend Developer</span>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="{{ asset('images/WhatsApp Image 2022-12-14 at 23.11.19.jpeg') }}" alt="team" />
							
						</div>
						<div class="team-info">
							<h3>Eddu Saldarriaga</h3>
							<span>Scrum Master</span>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-team-box">
						<div class="team-image">
							<img src="{{ asset('images/Screenshot_8.png') }}" alt="team" />
							
						</div>
						<div class="team-info">
							<h3>Leif Wither</h3>
							<span>Frontend Developer</span>
						</div>
					</div>
				</div>
			
			</div>
		</div>
	</section>
	<!-- End Team Section -->
	
	<!-- Start Testimonial Section -->
	
	<!-- End Testimonial Section -->
	
	<!-- Start Hire Section -->
	<section class="hire-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2 col-md-12">
					<div class="hire-content">
						<h2>¿Tienes algun proyecto en Mente?</h2>
						<p>?</p>
						<div class="hire-btn">
							<a class="default-btn-one" href="contact.html">Contactanos <span></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Hire Section -->
	
	<!-- Start Blog Section -->
	<section class="blog-section pt-100 pb-70">
		<div class="container">
        <div class="section-title">
            <h1>Mapa</h1>
            <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1994.9056073657796!2d-78.4699519!3d-0.1081339!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2sec!4v1672291599525!5m2!1ses!2sec" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
			<div class="section-title">
            <div class="chat">
                <div class="contamos">
                    <h1>
                        Chats

                    </h1>
                    <p>
                        Contamos con un chat en linea en el cual nos podremos poner en contacto con el profesionista
                        llegando asi aun acuerdo.
                    </p>
                </div>
                <div class="chathablante">
                    <div class="headerchat">
                    <img src="{{ asset('images/retrato-joven-carpintero-motivado-pie-junto-maquina-trabajar-madera-su-taller-carpinteria_342744-823.webp') }}" alt="">
                    </div>
                    <div class="chatmensajes">
                        <div class="mensajeenviado"> 
                            <img src="{{ asset('images/retrato-joven-carpintero-motivado-pie-junto-maquina-trabajar-madera-su-taller-carpinteria_342744-823.webp') }}" alt="">
                            <p>Hola estimado vi su publicacion, le puedo ayudar</p>      
                        </div>
                        <div class="mensajerecibido">
                           
                            <p>Si claro, cuanto seria el precio por arreglar la silla</p>   
                            <img src="{{ asset('images/De-emprendedor-a-empresario.webp') }}" alt="">

                        </div>
                        <div class="mensajeenviado">
                            <img src="{{ asset('images/retrato-joven-carpintero-motivado-pie-junto-maquina-trabajar-madera-su-taller-carpinteria_342744-823.webp') }}" alt="">
                            <p>Seria de 15 dolares</p>   
                        </div>
                        <div class="mensajerecibido">
                           
                            <p>Le puedo dar 10</p>   
                            <img src="{{ asset('images/De-emprendedor-a-empresario.webp') }}" alt=""> 
                        </div>
                        <div class="acepto">
                            <div class="trato">
                                <img src="{{ asset('images/retrato-joven-carpintero-motivado-pie-junto-maquina-trabajar-madera-su-taller-carpinteria_342744-823.webp') }}" alt="">
                                <p>Concluir trato por 10$</p>
                            </div>
                          
                            <div class="aceptar">
                                Aceptar
                            </div>
                            <div class="rechazar">
                                Rechazar
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
			</div>
			
			</div>
		</div>
	</section>
	<!-- End Blog Section -->
	
	
	<!-- End Partner Logo Section -->
	<!-- Start Footer Section -->
	<section class="footer-area section-padding">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<div class="footer-heading">
							<h3>Sobre Nosotros</h3>
						</div>
						
						
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<div class="footer-heading">
							<h3>Servicios</h3>
						</div>
						
					</div>
				</div>
				
				<div class="col-lg-4 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<div class="footer-heading">
							<h3>Cotacto</h3>
						</div>
						<div class="footer-info-contact">
							<span><strong>Telefono:</strong> <a href="tel:0802235678"></a></span>
						</div>
						<div class="footer-info-contact">
							<span><strong>Email:</strong> <a href="/cdn-cgi/l/email-protection#6f0b0a02002f0a170e021f030a410c0002"><span class="__cf_email__" data-cfemail="7e1a1b13113e1b061f130e121b501d1113"></span></a></span>
						</div>
					
						<!-- Start Subscribe Area -->
						
						<!-- End Subscribe Area -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Footer Section -->
	
	<!-- Start Copy Right Section -->
	<div class="copyright-area">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-6 col-md-6">
					<p>  2022 ComuService</p>
				</div>
				<div class="col-lg-6 col-md-6">
					<ul>
						<li> <a href="terms-condition.html">Terminos Y Condiciones</a>
						</li>
					
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- End Copy Right Section -->
	
	<!-- Start Go Top Section -->

	<!-- End Go Top Section -->
	
	<!-- jQuery Min JS -->
	<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="jquery.min.js"></script>
	<!-- Bootstrap Min JS -->
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<!-- MeanMenu JS  -->
	<script src="{{ asset('js/jquery.meanmenu.js') }}"></script>
	<!-- Appear Min JS -->
	<script src="{{ asset('js/jquery.appear.min.js') }}"></script>
	<!-- CounterUp Min JS -->
	<script src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
	<script src="{{ asset('js/jquery.counterup.min.js') }}"></script>
	<!-- Owl Carousel Min JS -->
	<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
	<!-- WOW Min JS -->
	<script src="{{ asset('js/wow.min.js') }}"></script>
	<!-- Main JS -->
	<script src="{{ asset('js/main.js') }}"></script>
	
</body>

</html>