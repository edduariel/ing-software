@include('header')
<br>
<br>
<br>
<div class="modal-body">
    <form action="{{ route('peticion.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">Titulo:</label>
            <input type="text" class="form-control" id="recipient-name" name="titulo">
        </div>
        <div class="form-group">
            <label for="message-text" class="col-form-label">Descripcion:</label>
            <input type="text" class="form-control" id="recipient-name" name="descripcion">
        </div>
        <div class="form-group">
            <label for="message-text" class="col-form-label">Precio:</label>
            <input type="number" class="form-control" id="recipient-name" name="precio">
        </div>
        <div class="form-group">
            <input type="file" class="form-control" id="recipient-name" name="fotos">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Crear</button>
        </div>
    </form>
</div>
</div>
