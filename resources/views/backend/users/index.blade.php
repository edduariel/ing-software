@extends('adminlte::page')
@extends('layout.general')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Lista de Usuarios</h1>
@stop

@section('content')
@if (session('info'))


<div class="alert alert-secondary" role="alert">
{{session('info')}}
</div>
    
@endif
    @livewire('users-index')
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop