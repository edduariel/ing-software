@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Asignar un Rol</h1>
@stop

@section('content')
@if (session('info'))
    <div class="alert alert-success">
        <strong>{{session('info')}}</strong>
    </div>
    
@endif
<form action="{{route('backend.users.update',$user )}}" method="POST">
    @csrf
    @method('put')
<div class="card">
    <div class="card-body">
        <p class="h5">Nombre:</p>
        <input class="form-control" name="nombre" value="{{$user->nombre}}">
        <p class="h5">Apellido:</p>
        <input class="form-control" name="apellido" value="{{$user->apellido}}">
        <p class="h5">Email:</p>
        <input class="form-control" name="email" value="{{$user->email}}">
        <p class="h5">Telefono:</p>
        <input class="form-control" name="telefono" value="{{$user->telefono}}">

    </div>
</div>
</form>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop