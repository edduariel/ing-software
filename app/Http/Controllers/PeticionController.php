<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Peticion_cliente;
use Illuminate\Support\Facades\Storage;

class PeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
         //dd( $productos);
        return view ('servicios');
    }
    public function indexServicio()
    {
        $peticion_clientes = \DB::table('peticion_clientes')
        ->select('peticion_clientes.*')
        ->get();
         //dd( $productos);
        return view ('serviciotodo')-> with('peticion_clientes',$peticion_clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Peticion_cliente $peticion)
    {
        $peticion=new Peticion_cliente();
        $peticion->titulo=$request->titulo;
        $peticion->descripcion=$request->descripcion;
        $peticion->precio=$request->precio;
        if ($request->hasfile('fotos')) {
            $peticion['fotos'] = $request->file('fotos')->store('uploads','public'); 
        }   
        $peticion->save();
        return redirect()->to('/servicios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Peticion_cliente $id)
    {
        $peticion_clientes = Peticion_cliente::find($id);
        //dd( $producto);
        return view ('comprarproducto',compact('peticion_clientes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
