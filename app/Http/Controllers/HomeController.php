<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
  
    public function indexBackend()
    {
        return view('homebackend');
    }
    public function indexHome()
    {
        return view('homemain');
    }
    

}
